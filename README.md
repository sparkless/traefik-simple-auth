# Traefik Simple Forward Authentication

This repository contains
 
* Django application that authenticates users
* Docker image to launch the app with a default credentials
* Kubernetes manifest file to use the authentication app with Traefik on Kubernetes

# Usage

To deploy to Kubernetes:

* Create the Ingress

```bash
kubectl create -f deploy/ingress.yaml
```

* Create the Forward Authentication service and deployment

```bash
kubectl create -f deploy/traefik.yaml
```

The default credentials are root/dummy123