from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, redirect_to_login
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
import logging, os

from django.views import View

logger = logging.getLogger(__name__)


class TraefikLoginView(LoginView):

    def get(self, request, *args, **kwargs):
        logger.debug(f'TraefikLoginView::get -- {request.META}')
        return super().get(request, *args, **kwargs)

    def form_invalid(self, form):
        logger.debug(f'TraefikLoginView::form_invalid -- {form.request.META}')
        res = super().form_invalid(form)
        res.status_code = 401
        return res

    def get_redirect_url(self):

        redir_url = self.request.POST.get(
            self.redirect_field_name,
            self.request.GET.get(self.redirect_field_name, '')
        )

        logger.debug(f'TraefikLoginView::get_redirect_url -- redir_url: {redir_url}')
        return redir_url

class AuthView(LoginRequiredMixin, View):
    login_url = "/login/"
    redirect_field_name = 'next'
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        logger.debug(f'AuthView::get -- {request.META}')

        return HttpResponse(status=200)

    def handle_no_permission(self):
        logger.debug(f'AuthView::handle_no_permission -- {self.request.META}')

        if self.raise_exception or self.request.user.is_authenticated:
            logger.debug(f'AuthView::handle_no_permission raising exception : {self.raise_exception} or User is Authenticated: ${self.request.user.is_authenticated}')
            raise PermissionDenied(self.get_permission_denied_message())

        if "HTTP_X_FORWARDED_HOST" in self.request.META and "HTTP_X_FORWARDED_PROTO" in self.request.META:
            redirect_url = f'{self.request.META["HTTP_X_FORWARDED_PROTO"]}://{self.request.META["HTTP_X_FORWARDED_HOST"]}'

            if os.getenv('USE_FORWARDED_URI',None):
                from django.http import HttpResponseRedirect
                next_uri = self.request.META['HTTP_X_FORWARDED_URI'] if 'HTTP_X_FORWARDED_URI' in self.request.META else self.request.META['RAW_URI']
                redirect = HttpResponseRedirect(f'{redirect_url}{self.get_login_url()}?next={next_uri}')
                logger.debug(f'Redirecting using FORWARDED_URI: {redirect}')
                return redirect

            redir =  redirect_to_login(redirect_url, self.get_login_url(), self.get_redirect_field_name())
            logger.debug(f'Redirecting as usual: {redir}')
            return redir

        logger.debug(f'AuthView::handle_no_permission Redirecting: ${self.request.get_full_path()}, {self.get_login_url()}, {self.get_redirect_field_name()}')

        return redirect_to_login(self.request.get_full_path(), self.get_login_url(), self.get_redirect_field_name())
